TARGET_DIR=Executables

CUDA_TOOLKIT_ROOT = </....../cuda-6.0>
OPENCV_ROOT= </......./opencv>
CUDA_LIBS_PATH = <$(CUDA_TOOLKIT_ROOT)/......./lib>
OPENCV_INCLUDE_ROOT=$(OPENCV_ROOT)/...../include
OPENCV_LIB_PATH=$(OPENCV_ROOT)/...../libs

GCC = g++
NVCC = $(CUDA_TOOLKIT_ROOT)/bin/nvcc -ccbin $(GCC) -m32 -arch=sm_30 -use_fast_math -O3

OBJS = $(TARGET_DIR)/main.o $(TARGET_DIR)/cuda_initialization.o $(TARGET_DIR)/cuda_memCopys.o $(TARGET_DIR)/cuda_GlobalWorld.o $(TARGET_DIR)/snapshotStructure.o $(TARGET_DIR)/cameraStructure.o $(TARGET_DIR)/globalWorldStructure.o $(TARGET_DIR)/localWorldStructure.o $(TARGET_DIR)/cuda_LocalWorld.o
PINCS = -I3rd_party -Iheaders -I$(CUDA_TOOLKIT_ROOT)/include -I$(OPENCV_INCLUDE_ROOT)
PLIBS = -L$(CUDA_LIBS_PATH) -L$(OPENCV_LIB_PATH)

COMPILER_FLAGS = -O3 -Xlinker -fpic -fexceptions -ffunction-sections -funwind-tables \
  -fstack-protector -no-canonical-prefixes -hardfp \
  -fpermissive -fexceptions -fomit-frame-pointer -fno-strict-aliasing \
  -finline-limit=64  

LIBS = -lcudart -lopencv_core -lopencv_highgui -lopencv_imgproc

$(TARGET_DIR)/%.o: src/%.cpp
	@echo '==============================='
	@echo 'Building file: $<'
	@echo 'Invoking: gcc'
	$(GCC) $(COMPILER_FLAGS) $(PINCS) --compile -x c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo '==============================='

$(TARGET_DIR)/%.o: src/structures/%.cpp
	@echo '==============================='
	@echo 'Building file: $<'
	@echo 'Invoking: gcc'
	$(GCC) $(COMPILER_FLAGS) $(PINCS) --compile -x c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo '==============================='

$(TARGET_DIR)/%.o: src/CUDA/%.cu
	@echo '==============================='
	@echo 'Building file: $<'
	@echo 'Invoking: nvcc'
	$(NVCC) $(PINCS) --device-w -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo '==============================='

$(TARGET_DIR)/GeoRefCUDA_V1: $(OBJS)
	@echo '==============================='
	@echo 'Creating target: $@'
	@echo 'Invoking: linker'
	$(GCC) $(PLIBS) $(LIBS) $(PINCS) $+ -o $@
	@echo 'Finished building target'
	@echo '==============================='

clean:
	rm -f $(TARGET_DIR)/*.o $(TARGET_DIR)/GeoRefCUDA_V1

all: clean $(OBJS) $(TARGET_DIR)/GeoRefCUDA_V1
