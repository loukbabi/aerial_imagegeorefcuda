1)
Fill the variables:
	CUDA_TOOLKIT_ROOT
	OPENCV_ROOT
	CUDA_LIBS_PATH
	OPENCV_INCLUDE_ROOT
	OPENCV_LIB_PATH
in the Makefile



2)Run the application

3)See the output in the "output folder"

Notes:
-The files in the "inputSnapshots/dataset" folder are the input of the application.
-The file "inputSnapshots/dataset/executionLog.txt" is the file determining the ids of the images that are going to be processed
-The file "headers/defines.h" is the file determing the required properties for the execution
