#ifndef __CUDA_WRAPPER_H_
#define __CUDA_WRAPPER__
#include <cuda_runtime.h>
#include <cuda.h>

//Initialization
extern "C" void gpuPowerON(cudaStream_t *stream1);
extern "C" void gpuPowerOFF(cudaStream_t *stream1);
extern "C" bool gpuAllocate2D(void** D_array, int width, int height, int bytes, size_t *stride);
extern "C" void gpuDeallocate2D(void** D_array);
extern "C" void gpuDeallocate(void** D_array);
//Texture Memory
extern "C" bool TextureBindingBilinearInterp(float* D_Texture_data, const float * __restrict__ H_data, const int width, const int height, size_t *stride);

//MemCopys
// extern "C" bool hostTOgpu2D(void *host_array, const void* device_array, int width, int height, int bytes, size_t stride, size_t host_stride);
// extern "C" bool gpuTOhost2D(const void *host_array, void* device_array, int width, int height, int bytes, size_t stride, size_t host_stride);
extern "C" bool gpuTOhost2D(void *host_array, const void* device_array, int width, int height, int bytes, size_t stride, size_t host_stride);
extern "C" bool hostTOgpu2D(const void *host_array, void* device_array, int width, int height, int bytes, size_t stride, size_t host_stride);

//Setters
extern "C" void gpuSetConstantUC(unsigned char *D_array, unsigned char constant, int width, int height, size_t stride, cudaStream_t *stream);
extern "C" void gpuSetConstantUI(unsigned int *D_array, unsigned int constant, int width, int height, size_t stride, cudaStream_t *stream);
extern "C" void gpuSetConstantFLOAT(float *D_array, float constant, int width, int height, size_t stride, cudaStream_t *stream);
extern "C" void gpuSetConstantSI(int *D_array, int constant, int width, int height, size_t stride, cudaStream_t *stream);

//Setters to Constant Memory
extern "C" void cuda_C2fW_cpy2Const(float *H_ProjMatrix);
extern "C" void cuda_W2C_cpy2Const(float *H_ProjMatrix);

//Functions for Global World
extern "C" void cuda_combineRGB(unsigned char *D_img, unsigned int *D_imgR, unsigned int *D_imgG, unsigned int *D_imgB, unsigned int *G_pointCounter,
								int height, int width, size_t CombImgStride, size_t imgStride, cudaStream_t *stream);

//Functions for Local World
extern "C" void cuda_cam2flatWorld_proj(float *D_x, float *D_y, size_t stride, cudaStream_t *stream);
extern "C" void cuda_Heights_interp(float *D_InterpHeights, float *x, float *y,
									float camPosX, float worldDimX, float camPosY, float worldDimY,
									size_t D_InterpHeights_stride, cudaStream_t *stream);

//Main CUDA function
extern "C" void cuda_AddOrthoImg2Global(unsigned int* G_red, unsigned int* G_green, unsigned int* G_blue, unsigned char* L_img, unsigned int* G_pointCounter,
										int* px1, int* px2, int* L_maxHeight, int* L_z,
										float *Wz, const size_t stride_1ch, const size_t stride_3ch, 
										const float camPosX, const float worldDimX, const float camPosY, const float worldDimY, const float GSD,
										const float minWorldPointX, const float minWorldPointY,
										cudaStream_t *stream);

// extern "C" void gpuDeallocate(unsigned char **undistortedImg_D, unsigned char **inputImg_D, unsigned char **scaledImg_D,
//  			int **map_D, int **tempImg_D, float **weight_x_D, float **weight_y_D, cudaStream_t *stream1);

// extern "C" void undistortionOnGPU(int *ind_luD, unsigned char *inputI, unsigned char *outputI,
// 			 float *alpha_x, float *alpha_y, cudaStream_t *stream1);

// extern "C" void pyrDownOnGPU(unsigned char *outputI_D, unsigned char *scaledImg_D, int *tempImg_D,
// 			cudaStream_t *stream1);

#endif