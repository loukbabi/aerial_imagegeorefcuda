#include "CUDA/cuda_wrapper.h"


template <typename T>
class cuda_array
{
private:
	T* _Darray;
	int _height, _width, _depth, _size, _bytes;
	size_t _stride;

public:
	
	inline T* data()
	{
		return _Darray;
	}

	inline size_t stride()
	{
		return _stride;
	}

	bool allocate(int height, int width, int depth = 1)
	{
		_size = height * width * depth;
		_height = height;
		_width = width;
		_depth = depth;
		_bytes = sizeof(T);

		return gpuAllocate2D((void**)&_Darray, _width, _height, _bytes, &_stride);

	}
	void deallocate()
	{
		gpuDeallocate2D((void**)&_Darray);
	}

	bool getTOhost(T* host_array, int host_stride)
	{
		return gpuTOhost2D((void*) host_array, (void*) _Darray, _width, _height, _bytes, _stride, host_stride);
	}

	void setConstant(unsigned int constant, cudaStream_t *stream)
	{
		gpuSetConstantUI(_Darray, constant, _width, _height, _stride, stream);
	}
	void setConstant(unsigned char constant, cudaStream_t *stream)
	{
		gpuSetConstantUC(_Darray, constant, _width, _height, _stride, stream);
	}
	void setConstant(float constant, cudaStream_t *stream)
	{
		gpuSetConstantFLOAT(_Darray, constant, _width, _height, _stride, stream);
	}
	void setConstant(int constant, cudaStream_t *stream)
	{
		gpuSetConstantSI(_Darray, constant, _width, _height, _stride, stream);
	}

	cuda_array()  {};
	~cuda_array() {};
	
};

class cuda_local_img
{
	int _width, _height, _bytes, _CombImgwidth;
	size_t _CombImgStride, _imgStride;
	bool createdConbinedImg;

public:
	cuda_array<unsigned char> D_imgR, D_imgG, D_imgB;
	cuda_array<unsigned char> D_img;

	inline unsigned char* data()
	{
		return D_img.data();
	}
	inline size_t stride()
	{
		return D_img.stride();
	}

	bool allocate(int height, int width)
	{
		createdConbinedImg = false;
		_width = width;
		_CombImgwidth = width*3;
		_height = height;
		_bytes = sizeof(unsigned char);

		bool flag = true;
		flag = flag && D_imgR.allocate(height, width);
		flag = flag && D_imgG.allocate(height, width);
		flag = flag && D_imgB.allocate(height, width);
		flag = flag && D_img.allocate(height, _CombImgwidth);

		_CombImgStride = D_img.stride();
		_imgStride = D_imgR.stride();

		return flag;
	}
	void deallocate()
	{
		D_imgR.deallocate();
		D_imgG.deallocate();
		D_imgB.deallocate();
		D_img.deallocate();
	}

	inline bool getTOhost(unsigned char* host_array, int host_stride)
	{
		return gpuTOhost2D((void*) host_array, (void*) D_img.data(), _CombImgwidth, _height, _bytes, _CombImgStride, host_stride) && createdConbinedImg;
	}

	bool sendToGPU(unsigned char* host_array, int host_stride)
	{
		createdConbinedImg = true;
		return hostTOgpu2D((void*) host_array, (void*) D_img.data(), _CombImgwidth, _height, _bytes, _CombImgStride, host_stride);
	}

	// void combineRGB(cudaStream_t *stream)
	// {
	// 	createdConbinedImg = true;
	// 	cuda_combineRGB(D_img.data(), D_imgR.data(), D_imgG.data(), D_imgB.data(), _height, _width, _CombImgStride, _imgStride, stream);
	// }

	void setConstantCannels(unsigned char red, unsigned char green, unsigned char blue, cudaStream_t *stream)
	{
		D_imgR.setConstant(red, stream);
		D_imgG.setConstant(green, stream);
		D_imgB.setConstant(blue, stream);
	}

	void setConstant(unsigned char constant, cudaStream_t *stream)
	{
		createdConbinedImg = true;
		D_img.setConstant(constant, stream);
	}

	cuda_local_img()   {};
	~cuda_local_img()  {};
	
};

class cuda_global_img
{
	int _width, _height, _bytes, _CombImgwidth, _CombImgbytes;
	size_t _CombImgStride, _imgStride;
	bool createdConbinedImg;

public:
	cuda_array<unsigned int> D_imgR, D_imgG, D_imgB;
	cuda_array<unsigned char> D_img;

	inline unsigned char* data()
	{
		return D_img.data();
	}
	inline size_t stride()
	{
		return D_img.stride();
	}

	bool allocate(int height, int width)
	{
		createdConbinedImg = false;
		_width = width;
		_CombImgwidth = width*3;
		_height = height;
		_bytes = sizeof(unsigned int);
		_CombImgbytes = sizeof(unsigned char);

		bool flag = true;
		flag = flag && D_imgR.allocate(height, width);
		flag = flag && D_imgG.allocate(height, width);
		flag = flag && D_imgB.allocate(height, width);
		flag = flag && D_img.allocate(height, _CombImgwidth);

		_CombImgStride = D_img.stride();
		_imgStride = D_imgR.stride();

		return flag;
	}
	void deallocate()
	{
		D_imgR.deallocate();
		D_imgG.deallocate();
		D_imgB.deallocate();
		D_img.deallocate();
	}

	inline bool getTOhost(unsigned char* host_array, int host_stride)
	{
		return gpuTOhost2D((void*) host_array, (void*) D_img.data(), _CombImgwidth, _height, _CombImgbytes, _CombImgStride, host_stride) && createdConbinedImg;
	}

	bool sendToGPU(unsigned char* host_array, int host_stride)
	{
		createdConbinedImg = true;
		return hostTOgpu2D((void*) host_array, (void*) D_img.data(), _CombImgwidth, _height, _CombImgbytes, _CombImgStride, host_stride);
	}

	void combineRGB(unsigned int *G_pointCounter, cudaStream_t *stream)
	{
		createdConbinedImg = true;
		cuda_combineRGB(D_img.data(), D_imgR.data(), D_imgG.data(), D_imgB.data(), G_pointCounter, _height, _width, _CombImgStride, _imgStride, stream);
	}

	void setConstantCannels(unsigned int red, unsigned int green, unsigned int blue, cudaStream_t *stream)
	{
		D_imgR.setConstant(red, stream);
		D_imgG.setConstant(green, stream);
		D_imgB.setConstant(blue, stream);
	}

	void setConstant(unsigned char constant, cudaStream_t *stream)
	{
		createdConbinedImg = true;
		D_img.setConstant(constant, stream);
	}

	cuda_global_img()   {};
	~cuda_global_img()  {};
	
};