#define DEG2RAD(x) (x * 0.017453292519943)
#define RAD2DEG(x) (x * 57.295779513082323)

#define USE_REAL_WORLD_HEIGHTMAP 0

#if  USE_REAL_WORLD_HEIGHTMAP == 0

	#define USE_NOISY_INPUT 1 
			
	#if USE_NOISY_INPUT == 0
		#define MAP_ACCURACY_X 26.9727
		#define MAP_ACCURACY_Y 26.976
		#define REAL_WORLD_HEIGHTMAP_PATH "inputSnapshots/dataset/RealWorldHeights.txt"
		#define OUTPUT_FOLDER "output/dataset"
		#define INPUT_IMAGE_FOLDER "inputSnapshots/dataset"
	#elif USE_NOISY_INPUT == 1
		#define MAP_ACCURACY_X 26.9727
		#define MAP_ACCURACY_Y 26.976
		#define REAL_WORLD_HEIGHTMAP_PATH "inputSnapshots/dataset/RealWorldHeights.txt"
		#define OUTPUT_FOLDER "output/dataset"
		#define INPUT_IMAGE_FOLDER "inputSnapshots/dataset"
	#endif

	#define CELL_SIZE 0.0000035
	#define WIDTH 512
	#define HEIGHT 512

	#define MAP_EXTRA_OFFSET 300	//offset for not falling out of the map
#endif

#define CELL_SIZE_INV (1/CELL_SIZE)
#define EXECUTION_LOG_FILE_NAME "executionLog.txt" //file containing the sequence of input images
#define IMG_FORMAT 		"png" 	//input image format
#define METADATA_FORMAT "txt" 	//input metadata format
#define POINT_CLOUD_FORMAT "off"//compatible with MeshLab

#define USE_USERS_GSD 0			//0-> false, 1->true
#define USER_GSD 0.0414214		//sample value
#define PRINT_WITH_MAX_GSD 1	//0-> false (minGSD), 1-> true (maxGSD)

#define STORE_GLOBAL_POINT_CLOUD 1

#define USE_HEIGHT_PRIORITY 1
