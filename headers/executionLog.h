#include "defines.h"


#include <stdio.h>

int readNumOfImgs()
{
	FILE * pFile;

	char filePath[128];
	sprintf(filePath, "%s/%s", INPUT_IMAGE_FOLDER, EXECUTION_LOG_FILE_NAME);
	pFile = fopen (filePath,"r");

	char temp[32];
	int numOfImgs;
	if(pFile)
	{
		fscanf (pFile, "%s %d", temp, &numOfImgs);
		if(numOfImgs==0)
			std::cout<<"No images in the execution sequence."<<std::endl;
	}else
	{
		std::cout<<"Couldn't open executionLog file."<<std::endl;
		return -1;
	}


	fclose (pFile);
	return numOfImgs;
}

void readExecutionLog(int* inputImageNumbers)
{
	FILE * pFile;
	char filePath[128];
	sprintf(filePath, "%s/%s", INPUT_IMAGE_FOLDER, EXECUTION_LOG_FILE_NAME);
	pFile = fopen (filePath,"r");

	if(pFile)
	{
		char temp[32];
		int numOfImgs;
		fscanf (pFile, "%s %d\n", temp, &numOfImgs);
		fscanf (pFile, "%s\n", temp);
		for(int i = 0; i < numOfImgs; i++)
			fscanf (pFile, "%d\n", &inputImageNumbers[i]);
	}else
	{
		std::cout<<"Couldn't open executionLog file."<<std::endl;
	}


	fclose (pFile);
}
