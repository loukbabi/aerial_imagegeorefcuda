// #include "testers/write2txt.hpp"
//add bicubic?

void bilinearInterpolation( Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &x,
		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &y,
		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &intepElevation,
		float minX, float maxX, float minY, float maxY,
		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &elevation )
{

	float c00, c10, c01, c11, wX, wY, gx, gy;
	int gxInt, gyInt;

	int height = intepElevation.rows();
	int heightE = elevation.rows() - 2;
	int width = intepElevation.cols();
	int widthE = elevation.cols() - 2;


    for (int i = 0; i < height; i++)
    	for (int j = 0; j < width; j++)
    	{
    		gx = ( ((x(i,j) - minX) / float(maxX - minX)) * heightE ) + 2;  // + 1 due to the offset
			gy = ( ((y(i,j) - minY) / float(maxY - minY)) * widthE ) + 2;   // + 1 due to the offset
    		gxInt = ceil(gx);
    		gyInt = ceil(gy);

//    		if(gxInt >= widthE) // at boundaries
//    			gxInt--;
//
//    		if(gyInt >= heightE)// at boundaries
//    			gyInt--;

//    		std::cout<<gyInt<<" "<<gxInt<<" - "<<i<<" "<<j<<" == "<<elevation.rows()<<" "<<elevation.cols()<<" == "<<minY<<std::endl;

    		c00 = elevation(gyInt - 1, gxInt - 1);
    		c10 = elevation(gyInt - 1, gxInt    );
    		c01 = elevation(gyInt    , gxInt - 1);
    		c11 = elevation(gyInt    , gxInt    );

    		wX = gxInt - gx;
    		wY = gyInt - gy;
    		intepElevation(i,j) = (1.0 - wX) * (1.0 - wY) * c00 +
									wX * (1.0 - wY) * c10 +
									(1.0 - wX) * wY * c01 +
									wX * wY * c11;
    	}

}

void nearestInterpolation( Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &x,
				Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &y,
				Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &interpElevation,
				float minX, float maxX, float minY, float maxY,
				Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &elevation)
{
// By using normalizationX and normalizationY we fall into a very special case of bug. Float accuracy is not enough.
// By doing normalizationX = (elevation.cols() - 1) / float(maxX - minX); and normalizationY = (elevation.rows() - 1) / float(maxY - minY);
// and after that use the gx = (x(i,j) - minX) * normalizationX; and gy = (y(i,j) - minY) * normalizationY;
// the output falls out of the elevation.sizes() boundaries in some cases.
// On the other hand, by doing float gx2 = ((x(i,j) - minX) / float(maxX - minX)) * (elevation.cols() - 1); and float gy2 = ((y(i,j) - minY) / float(maxY - minY)) * (elevation.rows() - 1);
// although the math are the same, the output does not fall out of the elevation.sizes() boundaries. :O :O :O



//	float normalizationX = (elevation.cols() - 1) / float(maxX - minX);
//	float normalizationY = (elevation.rows() - 1) / float(maxY - minY);

	float heightE = elevation.cols() - 1.0;
	float widthE = elevation.rows() - 1.0;

	for (int i = 0; i < interpElevation.rows(); i++)
		for (int j = 0; j < interpElevation.cols(); j++)
		{
//			float gx = (x(i,j) - minX) * normalizationX;
//			float gy = (y(i,j) - minY) * normalizationY;

//			temp_normX(i,j) = gx;
//			temp_normY(i,j) = gy;

			float gx2 = ((x(i,j) - minX) / float(maxX - minX)) * heightE;
			float gy2 = ((y(i,j) - minY) / float(maxY - minY)) * widthE;

			int gxInt = ceil(gx2), gyInt = ceil(gy2);

//			std::cout<<gyInt<<" "<<gxInt<<" - "<<i<<" "<<j<<" == "<<elevation.rows()<<" "<<elevation.cols()<<" == "<<minY<<std::endl;
			interpElevation(i,j) = elevation(gyInt, gxInt);
		}
}

void bilinearInterpColor(uchar* outputImg, uchar* inputImg, float gx, float gy, int i, int j, int width)
{
	gy = abs(gy); // the only occasion that we need the abs is for some values that are -1<value<0
	gx = abs(gx); // the only occasion that we need the abs is for some values that are -1<value<0

	int gyInt = int(gy), gxInt = int(gx);
	float wX = gx - gxInt, wY = gy - gyInt;

	float w00 = (1.0 - wX) * (1.0 - wY);
	float w10 = wX * (1.0 - wY);
	float w01 = (1.0 - wX) * wY;
	float w11 = wX * wY;

	//c00
	int inputPixelID = (gyInt * width + gxInt) * 3;
	float c = inputImg[ inputPixelID ];
	float blue = w00 * c;

	c = inputImg[ inputPixelID + 1 ];
	float green = w00 * c;

	c = inputImg[ inputPixelID + 2 ];
	float red = w00 * c;

	//c10
	inputPixelID = (gyInt * width + gxInt + 1) * 3;
	c = inputImg[ inputPixelID ];
	blue += w10 * c;

	c = inputImg[ inputPixelID + 1 ];
	green += w10 * c;

	c = inputImg[ inputPixelID + 2 ];
	red += w10 * c;

	//c01
	gyInt = (gyInt + 1) * width + gxInt; // gyInt combined with gxInt
	inputPixelID = (gyInt) * 3;
	c = inputImg[ inputPixelID ];
	blue += w01 * c;

	c = inputImg[ inputPixelID + 1 ];
	green += w01 * c;

	c = inputImg[ inputPixelID + 2 ];
	red += w01 * c;

	//c11
	inputPixelID = (gyInt + 1) * 3;
	c = inputImg[ inputPixelID ];
	blue += w11 * c;

	c = inputImg[ inputPixelID + 1 ];
	green += w11 * c;

	c = inputImg[ inputPixelID + 2 ];
	red += w11 * c;

	int ouputPixelID = (i * width + j) * 3;

	if(round(blue)>255 || round(green)>255 || round(red)>255)
	{
		std::cout<<blue<<" "<<green<<" "<<red<<" "<<std::endl;
		std::cout<<w00<<" "<<w01<<" "<<w10<<" "<<w11<<std::endl;
		std::cout<<wX<<" "<<wY<<std::endl;
		std::cout<<gy<<" "<<gyInt<<std::endl;
		int wait;
		std::cin>>wait;
	}

	outputImg[ ouputPixelID    ] = round(blue);
	outputImg[ ouputPixelID + 1] = round(green);
	outputImg[ ouputPixelID + 2] = round(red);


}




//void bilinearInterpolation( Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &x,
//		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &y,
//		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &intepElevation,
//		float minX, float maxX, float minY, float maxY,
//		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &elevation )
//{
//
//	float c00, c10, c01, c11, wX, wY, gx, gy;
//	int gxInt, gyInt;
//
//	int height = intepElevation.rows();
//	int heightE = elevation.rows() - 1;
//	int width = intepElevation.cols();
//	int widthE = elevation.cols() - 1;
//
//	float normalizationX = widthE / float(maxX - minX);
//	float normalizationY = heightE / float(maxY - minY);
//
//    for (int i = 0; i < height; i++)
//    	for (int j = 0; j < width; j++)
//    	{
//    		gx = ((x(i,j) - minX) * normalizationX); // 0.5 -> needed for the test world. Boxes cannot start and end at 500,500. In this case box_end=500 and box_start=501.
//			gy = ((y(i,j) - minY) * normalizationY); // so we move our world 0.5
//    		gxInt = int(gx);
//    		gyInt = int(gy);
//
//    		if(gxInt >= widthE) // at boundaries
//    			gxInt--;
//
//    		if(gyInt >= heightE)// at boundaries
//    			gyInt--;
//
//    		c00 = elevation(gyInt, gxInt);
//    		c10 = elevation(gyInt, gxInt+1);
//    		c01 = elevation(gyInt+1, gxInt);
//    		c11 = elevation(gyInt+1, gxInt+1);
//
//    		wX = gx - gxInt;
//    		wY = gy - gyInt;
//    		intepElevation(i,j) = (1.0 - wX) * (1.0 - wY) * c00 +
//									wX * (1.0 - wY) * c10 +
//									(1.0 - wX) * wY * c01 +
//									wX * wY * c11;
//    	}
//
//}
