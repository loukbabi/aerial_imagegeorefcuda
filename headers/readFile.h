#ifndef __READFILE_H__
#define __READFILE_H__

#include <stdio.h>
#include "defines.h"

inline bool readTXT(const char * filename, Eigen::Vector3f &orientation, Eigen::Vector3f &possition, float* FOV_angle)
{
	FILE * pFile;
	pFile = fopen (filename,"r");
	if(pFile)
	{
		fscanf (pFile, "%f ", &possition(0));
		float temp_possition1;
		fscanf (pFile, "%f ", &temp_possition1); // from this dataset and further we will use as input camera position metadata where y will grow going to North
		possition(1) = -temp_possition1;		 // then the algorithm will convert this input to y growing while going South (to much the image Y orientation)
		fscanf (pFile, "%f\n", &possition(2));
		fscanf (pFile, "%f ", &orientation(0));
		fscanf (pFile, "%f ", &orientation(1));
		fscanf (pFile, "%f\n", &orientation(2));
		fscanf (pFile, "%f", FOV_angle);
		return true;
	}else
	{
		std::cout<<filename<<" not found."<<std::endl;
		return false;
	}
	fclose (pFile);
}




#endif
