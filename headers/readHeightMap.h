
#include <stdio.h>
#include <Eigen/Dense>

//bool readTXT(const char * filename, Eigen::Vector3f &orientation, Eigen::Vector3f &possition, float* FOV_angle)
//{
//
//	FILE * pFile;
//	pFile = fopen (filename,"r");
//	if(pFile)
//	{
//		fscanf (pFile, "%f ", &possition(0));
//		fscanf (pFile, "%f ", &possition(1));
//		fscanf (pFile, "%f\n", &possition(2));
//		fscanf (pFile, "%f ", &orientation(0));
//		fscanf (pFile, "%f ", &orientation(1));
//		fscanf (pFile, "%f\n", &orientation(2));
//		fscanf (pFile, "%f", FOV_angle);
//		return true;
//	}else
//	{
//		std::cout<<filename<<" not found."<<std::endl;
//		return false;
//	}
//	fclose (pFile);
//}

char gPath[68];

inline bool readTXT_size2(const char *filename, int *sizeX, int *sizeY)
{
	FILE * pFile;
	pFile = fopen (filename,"r");
	if(pFile)
	{
		fscanf (pFile, "%d %d", sizeY, sizeX);
	}else
	{
		printf("%s not found.\n", filename);
		return false;
	}
	fclose (pFile);
	return true;
}

inline bool readTXT_size(const char * filename, int *size)
{
	char fullPath[128];
	sprintf(fullPath, "%s/%s3.txt", gPath, filename);

	FILE * pFile;
	pFile = fopen (fullPath,"r");
	if(pFile)
	{
		fscanf (pFile, "%d ", size);
	}else
	{
		printf("%s not found.\n", filename);
		return false;
	}
	fclose (pFile);
	return true;
}

inline bool readTXT2(int* matrix, const char * filename, int sizeX, int sizeY)
{
	char fullPath[128];
	sprintf(fullPath, "%s/%s3.txt", gPath, filename);

	FILE * pFile;
	pFile = fopen (fullPath,"r");
	int foo, foo2;
	fscanf (pFile, "%d %d", &foo, &foo2);
	if(pFile)
	{
		for(int i = 0; i < sizeY; i++)
			for(int j = 0; j < sizeX; j++)
				fscanf (pFile, "%d\n", &(matrix[i*sizeX + j]));
	}else
	{
		printf("%s not found.\n", filename);
		return false;
	}
	fclose (pFile);
	return true;
}

inline bool readTXT2(double* matrix, const char * filename, int sizeX, int sizeY)
{
	char fullPath[128];
	sprintf(fullPath, "%s/%s3.txt", gPath, filename);

	FILE * pFile;
	pFile = fopen (fullPath,"r");
	int foo, foo2;
	fscanf (pFile, "%d %d", &foo, &foo2);
	if(pFile)
	{
		for(int i = 0; i < sizeY; i++)
			for(int j = 0; j < sizeX; j++)
				fscanf (pFile, "%lf\n", &(matrix[i*sizeX + j]));
	}else
	{
		printf("%s not found.\n", filename);
		return false;
	}
	fclose (pFile);
	return true;
}

inline bool readTXT(int* matrix, const char * filename, int size)
{
	char fullPath[128];
	sprintf(fullPath, "%s/%s3.txt", gPath, filename);

	FILE * pFile;
	pFile = fopen (fullPath,"r");
	int foo;
	fscanf (pFile, "%d\n", &foo);
	if(pFile)
	{
		for(int i = 0; i < size; i++)
			fscanf (pFile, "%d\n", &(matrix[i]));
	}else
	{
		printf("%s not found.\n", filename);
		return false;
	}
	fclose (pFile);
	return true;
}

inline bool readTXT(double* matrix, const char * filename, int size)
{
	char fullPath[128];
	sprintf(fullPath, "%s/%s3.txt", gPath, filename);

	FILE * pFile;
	pFile = fopen (fullPath,"r");
	int foo;
	fscanf (pFile, "%d\n", &foo);
	if(pFile)
	{
		for(int i = 0; i < size; i++)
			fscanf (pFile, "%lf\n", &(matrix[i]));
	}else
	{
		printf("%s not found.\n", filename);
		return false;
	}
	fclose (pFile);
	return true;
}

inline bool readTXT2_eigen(Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> &matrix, const char * filename)
{

	FILE * pFile;
	pFile = fopen (filename,"r");
	int foo, foo2;
	fscanf (pFile, "%d %d\n", &foo, &foo2);
	if(pFile)
	{
		for(int i = 0; i < matrix.rows(); i++)
			for(int j = 0; j < matrix.cols(); j++)
				fscanf (pFile, "%f\n", &(matrix(i,j)));
	}else
	{
		printf("%s not found.\n", filename);
		return false;
	}
	fclose (pFile);
	return true;
}
