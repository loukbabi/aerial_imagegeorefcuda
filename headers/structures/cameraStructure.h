
class camera{


public:
	Eigen::Matrix<float, 11, 1> cameraValues;
	Eigen::Matrix<float, 3, 4> W2C;
	Eigen::Matrix3f C2fW;

	void setCameraValues(Snapshot &_snapshot, float flocaLength, float kWidth, float kHeight);

	void projectionMatrixC2W_flatWorld();

	void projectionMatrixW2C();
};
