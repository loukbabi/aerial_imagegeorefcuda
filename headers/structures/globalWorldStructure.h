#include "defines.h"
// #include "CUDA/structures/cudaStructure.h"

class elevationMap{

public:
	Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> heights;
	int tileSizeX, tileSizeY;
	int halfTileSizeX, halfTileSizeY;
	float accurasyX, accurasyY;
	float worldDimX, worldDimY;

	float *DT_heights;
	size_t DT_heights_stride;

	elevationMap();
	bool sendTOtextureMem(float* H_data/*, float* D_data*/, int width, int height);

};

class globalWorld{

public:
	float minWorldPointX, minWorldPointY, maxWorldPointY, maxWorldPointX;
	float minGSD, maxGSD, GSD;

	unsigned int *globalOrthoImageValidValuesCount;
	cv::Mat globalOrthoImage;

	// cuda_array<unsigned char> D_globalOrthoImageValidValues;
	cuda_array<unsigned int> D_globalOrthoImageValidValuesCount;
	
	cuda_global_img D_globalOrthoImage;

	Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> z;
	cuda_array<float> D_z;

	elevationMap testHeights;
	int globalValidOrthoValuesCount;

	// #if FILL_VOIDS
	// 	int firstGlobalValidLeft, firstGlobalValidDown;
	// 	int firstGlobalValidRight, firstGlobalValidUp;
	// #endif

	#if USE_STITCHED_FOR_POINT_CLOUD
		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> xGlobal, yGlobal, zGlobal;
	#endif


	bool allocateGlobalOutput(int width, int height, cudaStream_t *stream);
	
	void deallocateGlobalOutput();

	//Calculates the size of the global world (wont be needed in the actual implementation)
	bool globalWorldInitialization(Snapshot* inputSnapshotStream, int inputImgNum, cudaStream_t *stream);

	//Adds current orthorectified image to the global
	void addToGlobal(cv::Mat& localOrthoImg, camera& cam,
			Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& x,
			Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& y,
			Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& z,
			Eigen::Array<bool, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>& localOrthoImageValidValues);

	void copyPanoToGlobalImg(cv::Mat &pano);

	bool GetImgFromGPU(cudaStream_t *stream);

	bool storePointCloud();
};
