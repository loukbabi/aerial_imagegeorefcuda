#include "defines.h"

class worldPoints
{

public:
	Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> x, y, z;
	cuda_array<float> D_x, D_y, D_z;

	float minX, maxX;
	float minY, maxY;

	bool allocate(int sizeY, int sizeX);
	void deallocate();
};

class pixelIDs
{

public:

	Eigen::Array<int, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> x, y;
	cuda_array<int> D_x, D_y;

	bool allocate(int sizeY, int sizeX);
	void deallocate();
};

class projectedImg
{

public:
	pixelIDs perspIds;
	Eigen::Array<bool, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> validOrthoValues;
	cuda_array<unsigned char> D_validOrthoValues;

	#if USE_HEIGHT_PRIORITY
		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> maxElevation;
		cuda_array<float> D_maxElevation;
	#endif

	int firstValidLeft, firstValidRight, firstValidUp, firstValidDown;

	bool allocate(int sizeY, int sizeX);

	void deallocate();
};

class localWorld
{

public:
	worldPoints _worldPoints;
	camera _camera;
	projectedImg pixelsLog;
	cv::Mat orthoImage;
	// cuda_local_img D_orthoImage;
	cuda_array<int> D_px1, D_px2, L_maxHeight, L_z;

	bool allocate(Snapshot &_snapshot);
	void deallocate();

	void cam2flatWorld_proj(cudaStream_t *stream);
	void findFlatWorldArea(Snapshot &_snapshot);

	void test_heights_interp(elevationMap &worldHeights, cudaStream_t *stream);
	bool addHeights(elevationMap &worldHeights, cudaStream_t *stream);

	void sendProjMat2GPU();

	void AddOrthoImg2Global(globalWorld &_GlobalWorld, Snapshot &inSnapshot, cudaStream_t *stream);
};
