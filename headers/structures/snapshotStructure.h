#include <opencv/highgui.h>

// #include "CUDA/structures/cudaStructure.h"

class Metadata{


public:
	Eigen::Vector3f UAVpossition; 	//x,y,z (meters)
	Eigen::Vector3f UAVorientation;	// around x,y,z (degrees)
	float FOV_angle; 				//degrees
};


class Snapshot{

public:
	cv::Mat inputImg;
	Metadata inputMetadata;
	cuda_local_img D_inputImg;

	Snapshot();
	~Snapshot();

	bool readImg(const char* inputPath, int imgNum);

	bool readMetadata(const char* inputPath, int imgNum);

	bool readSnapshot(const char* inputPath, int imgNum);

	void releaseSnapshot();

	bool sendImgTOgpu();
};
