
#include <stdio.h>

inline void write2txt(Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> output, const char* filename)
{
	char fullPath[128];
	sprintf(fullPath, "tests/%s.txt", filename);
	FILE * pFile;
	pFile = fopen (fullPath,"w+");

	if(pFile)
	{
		fprintf(pFile, "%ld\n", output.rows());
		fprintf(pFile, "%ld\n", output.cols());
		for(int i = 0; i < output.rows(); i++)
			for(int j = 0; j < output.cols(); j++)
				fprintf (pFile, "%f\n", output(i,j));
		printf("File '%s' was written.\n", fullPath);
	}else
	{
		printf("Could not create '%s'\n", fullPath);
	}

	fclose (pFile);
}

inline void write2txt(Eigen::Array<bool, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> output, const char* filename)
{
	char fullPath[128];
	sprintf(fullPath, "tests/%s.txt", filename);
	FILE * pFile;
	pFile = fopen (fullPath,"w+");

	if(pFile)
	{
		fprintf(pFile, "%ld\n", output.rows());
		fprintf(pFile, "%ld\n", output.cols());
		for(int i = 0; i < output.rows(); i++)
			for(int j = 0; j < output.cols(); j++)
				fprintf (pFile, "%d\n", output(i,j));
		printf("File '%s' was written.\n", fullPath);
	}else
	{
		printf("Could not create '%s'\n", fullPath);
	}

	fclose (pFile);
}

inline void write2txt(unsigned int *output, int height, int width, const char* filename, const char *output_folder)
{
	char fullPath[128];
	sprintf(fullPath, "%s/%s.txt", output_folder, filename);
	FILE * pFile;
	pFile = fopen (fullPath,"w+");

	if(pFile)
	{
		fprintf(pFile, "%ld\n", height);
		fprintf(pFile, "%ld\n", width);
		for(int i = 0; i < height; i++)
			for(int j = 0; j < width; j++)
			{
				int id = i * width + j;
				fprintf (pFile, "%d\n", output[id]);
			}
		printf("File '%s' was written.\n", fullPath);
	}else
	{
		printf("Could not create '%s'\n", fullPath);
	}

	fclose (pFile);
}
