numOfimgs: 5
image_sequence_ids:
0
1
2
3
4

Explanation
============
numOfimgs-> the number of input images (this has to be always set)
image_sequence_ids-> the number ids of the images that will be used as inputs

2 Examples
=======
numOfimgs: 3
image_sequence_ids:
0
2
4
//we are going to use 3 images. The first will be the image named 00000000.png, the second 00000002.pgn and the third 00000005.png


numOfimgs: 3
image_sequence_ids:
3
1
4
//we are going to use 3 images. The first will be the image named 00000003.png, the second 00000001.pgn and the third 00000004.png

