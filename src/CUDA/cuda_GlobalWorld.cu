#include "CUDA/cuda_wrapper.h"
#include "CUDA/cuda_defines.h"
#include <stdio.h>

inline int iDivUp(int a, int b){ return ((a % b) != 0) ? (a / b + 1) : (a / b); }

__global__ void Kernel_combineRGB(unsigned char *D_img, unsigned int *D_imgR, unsigned int *D_imgG, unsigned int *D_imgB, unsigned int *G_pointCounter,
										int CombImgStride, int imgStride, int width, int height)
{
	int tid  = blockIdx.x * blockDim.x + threadIdx.x;
	int tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ((tid>=width) || (tidy>=height)) return;

	D_img += tidy*CombImgStride + tid*3;

	tid += tidy*imgStride;
	float scaling = 1.0f / float(G_pointCounter[tid]);

    D_img[0] = (unsigned char) round(float(D_imgB[tid]) * scaling);
    D_img[1] = (unsigned char) round(float(D_imgG[tid]) * scaling);
    D_img[2] = (unsigned char) round(float(D_imgR[tid]) * scaling);
}
void cuda_combineRGB(unsigned char *D_img, unsigned int *D_imgR, unsigned int *D_imgG, unsigned int *D_imgB, unsigned int *G_pointCounter,
						int height, int width, size_t CombImgStride, size_t imgStride, cudaStream_t *stream)
{
	dim3 dimGrid(iDivUp(width, BLOCKSIZE_GlobalWorld_x), iDivUp(height, BLOCKSIZE_GlobalWorld_y));
   	dim3 dimBlock(BLOCKSIZE_GlobalWorld_x, BLOCKSIZE_GlobalWorld_y);

   	Kernel_combineRGB<<<dimGrid, dimBlock, 0, (*stream)>>>(D_img, D_imgR, D_imgG, D_imgB, G_pointCounter, CombImgStride, (imgStride>>2), width, height);
}