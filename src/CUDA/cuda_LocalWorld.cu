#include "CUDA/cuda_wrapper.h"
#include "CUDA/cuda_defines.h"
#include "defines.h"

#include <stdio.h>

__constant__ float D_C2fW[9];
__constant__ float D_W2C[12];
texture<float,2,cudaReadModeElementType> d_texture_interp_float; 

void cuda_C2fW_cpy2Const(float *H_ProjMatrix)
{
    cudaMemcpyToSymbol(D_C2fW, H_ProjMatrix, 9 * sizeof(float));
}
void cuda_W2C_cpy2Const(float *H_ProjMatrix)
{
    cudaMemcpyToSymbol(D_W2C, H_ProjMatrix, 12 * sizeof(float));
}

bool TextureBindingBilinearInterp(float* D_Texture_data, const float * __restrict__ H_data, const int width, const int height, size_t *stride)
{
	cudaError_t cudaStatus;
    cudaStatus = cudaMallocPitch((void**)&D_Texture_data, stride, width * sizeof(float), height);
    if(cudaStatus != cudaSuccess)	{ printf("GPU Texture allocation error!!\n"); return false; }

    cudaChannelFormatDesc desc = cudaCreateChannelDesc<float>();
    cudaStatus = cudaBindTexture2D(0,&d_texture_interp_float, D_Texture_data, &desc, width, height, *stride);
    if(cudaStatus != cudaSuccess)	{ printf("GPU Texture binding error!!\n"); return false; }

    d_texture_interp_float.addressMode[0] = cudaAddressModeClamp;
    d_texture_interp_float.addressMode[1] = cudaAddressModeClamp;
    d_texture_interp_float.filterMode = cudaFilterModeLinear;   // --- Enable linear filtering
    d_texture_interp_float.normalized = true;                   // --- Texture coordinates will NOT be normalized
    cudaStatus = cudaMemcpy2D(D_Texture_data, *stride, H_data, sizeof(float)*width, sizeof(float)*width, height, cudaMemcpyHostToDevice);
    if(cudaStatus != cudaSuccess)	{ printf("GPU Texture copy error!!\n"); return false; }

    return true;
}

inline int iDivUp(int a, int b){ return ((a % b) != 0) ? (a / b + 1) : (a / b); }

__global__ void Kernel_cam2flatWorld_proj(float *D_x, float *D_y, int stride)
{
	const int tidx  = blockIdx.x * blockDim.x + threadIdx.x;
	const int tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ((tidx>=WIDTH) || (tidy>=HEIGHT)) return;

	float pt3 =  D_C2fW[2] * tidx + D_C2fW[5] * tidy + D_C2fW[8];
	float pt1 = (D_C2fW[0] * tidx + D_C2fW[3] * tidy + D_C2fW[6]) / pt3;
	float pt2 = (D_C2fW[1] * tidx + D_C2fW[4] * tidy + D_C2fW[7]) / pt3;

	const int tid = tidy*stride + tidx;

	D_x[tid] = pt1;
	D_y[tid] = pt2;
}

void cuda_cam2flatWorld_proj(float *D_x, float *D_y, size_t stride, cudaStream_t *stream)
{
	dim3 dimGrid(iDivUp(WIDTH, BLOCKSIZE_LocalWorld_x), iDivUp(HEIGHT, BLOCKSIZE_LocalWorld_y));
  	dim3 dimBlock(BLOCKSIZE_LocalWorld_x, BLOCKSIZE_LocalWorld_y);

   	Kernel_cam2flatWorld_proj<<<dimGrid, dimBlock, 0, (*stream)>>>(D_x, D_y, (stride>>2));
}


__global__ void Kernel_Heights_interp(float * __restrict__ d_result, float * __restrict__ x, float * __restrict__ y,
										   const float camPosX, const float worldDimX, const float camPosY, const float worldDimY,
                                           const int D_InterpHeights_stride)
{
   	const int tidx = threadIdx.x + blockDim.x * blockIdx.x;
   	const int tidy = threadIdx.y + blockDim.y * blockIdx.y;

   	if ( (tidx<WIDTH) && (tidy<HEIGHT) )
   	{
   		const int tid = tidy*D_InterpHeights_stride + tidx;

   		// d_result[tid] = tex2D(d_texture_interp_float, (float(x[tid])/xRange) - normRangeX, (float(y[tid])/yRange) - normRangeY);

		d_result[tid] = tex2D(d_texture_interp_float, 
						( (x[tid] + camPosX) / worldDimX ) + 0.5f, 
						( (y[tid] + camPosY) / worldDimY ) + 0.5f
						);   		
   	}
}
void cuda_Heights_interp(float *D_InterpHeights, float *x, float *y,
						float camPosX, float worldDimX, float camPosY, float worldDimY,
						size_t D_InterpHeights_stride, cudaStream_t *stream)
{
	dim3 dimGrid(iDivUp(WIDTH, BLOCKSIZE_LocalWorld_x), iDivUp(WIDTH, BLOCKSIZE_LocalWorld_y));
  	dim3 dimBlock(BLOCKSIZE_LocalWorld_x, BLOCKSIZE_LocalWorld_y);
  	printf("camPos %f, %f\n", camPosX, camPosY);
  	Kernel_Heights_interp<<<dimGrid, dimBlock, 0, (*stream)>>>(D_InterpHeights, x, y, camPosX, worldDimX, camPosY, worldDimY,
  																	(D_InterpHeights_stride>>2));
}









__global__ void Kernel_Projections(int *L_px1, int *L_px2, int *L_z, int *L_maxHeight,
									float *Wz, const size_t stride_1ch,
									const float camPosX, const float worldDimX, const float camPosY, const float worldDimY, const float GSD)
{
	const int tidx = blockIdx.x * blockDim.x + threadIdx.x;
	const int tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ((tidx>=WIDTH) || (tidy>=HEIGHT)) return;

	//Camera to flat world projection
	float Wpt3 =  D_C2fW[2] * tidx + D_C2fW[5] * tidy + D_C2fW[8];
	const float Wpt1 = (D_C2fW[0] * tidx + D_C2fW[3] * tidy + D_C2fW[6]) / Wpt3;
	const float Wpt2 = (D_C2fW[1] * tidx + D_C2fW[4] * tidy + D_C2fW[7]) / Wpt3;

	//Dem interpolation (Hardware interpolation using texture memory)
	Wpt3 = tex2D(	d_texture_interp_float, 
					( (Wpt1 + camPosX) / worldDimX ) + 0.5f, 
					( (Wpt2 + camPosY) / worldDimY ) + 0.5f
					);

	//World to camera projection
	const float px3 =       D_W2C[2] * Wpt1 + D_W2C[5] * Wpt2 + D_W2C[8] * Wpt3 + D_W2C[11] ;
	const int px1 = round( (D_W2C[0] * Wpt1 + D_W2C[3] * Wpt2 + D_W2C[6] * Wpt3 + D_W2C[9 ]) / px3 );
	const int px2 = round( (D_W2C[1] * Wpt1 + D_W2C[4] * Wpt2 + D_W2C[7] * Wpt3 + D_W2C[10]) / px3 );
	
	//Check if the pixel ID falls into image's dimentions
	const bool validPXvalue = (px1>=0) && (px1<WIDTH) && (px2>=0) && (px2<HEIGHT);

	Wz += int((round(Wpt2 / GSD) * stride_1ch + round(Wpt1 / GSD)) * validPXvalue);
	(*Wz) = Wpt3 = (Wpt3 * validPXvalue) + ((*Wz) * (!validPXvalue));

	int id = (tidy * stride_1ch + tidx);
	L_px1[id] = (px1 * validPXvalue) + ((!validPXvalue) * (-100));
	L_px2[id] = px2;

	id = (px2 * stride_1ch + px1) * validPXvalue;

	L_z[id] = int(Wpt3*1000000);

	L_maxHeight += id;
	atomicMax( L_maxHeight, int(Wpt3*1000000));
}

__global__ void Kernel_CreateOrthoImg(	unsigned int* G_red, unsigned int* G_green, unsigned int* G_blue, unsigned char* L_img, unsigned int* G_pointCounter,
									int* L_px1, int* L_px2, int* L_maxHeight, int* L_z,
									float GSD, const size_t stride_1ch, const size_t stride_3ch)
{
	const int tidx = blockIdx.x * blockDim.x + threadIdx.x;
	const int tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ((tidx>=WIDTH) || (tidy>=HEIGHT)) return;

	const float Wpt3 =  D_C2fW[2] * tidx + D_C2fW[5] * tidy + D_C2fW[8];
	const float Wpt1 = (D_C2fW[0] * tidx + D_C2fW[3] * tidy + D_C2fW[6]) / Wpt3;
	const float Wpt2 = (D_C2fW[1] * tidx + D_C2fW[4] * tidy + D_C2fW[7]) / Wpt3;

	int id = (tidy * stride_1ch + tidx);

	const int px1 = L_px1[id];
	const int px2 = L_px2[id];
	id = (px2 * stride_1ch + px1);	
	const bool validPXvalue = (px1 != -100) && (L_maxHeight[id] == L_z[id]);

	id = (round(Wpt2 / GSD) * stride_1ch + round(Wpt1 / GSD)) * validPXvalue;

	//Add to Global
	L_img += (px2 * stride_3ch + px1 * 3) * validPXvalue;
	G_blue[id]  += (unsigned int) (validPXvalue * L_img[0]);
	G_green[id] += (unsigned int) (validPXvalue * L_img[1]);
	G_red[id]   += (unsigned int) (validPXvalue * L_img[2]);
	G_pointCounter[id] += validPXvalue;
}

void cuda_AddOrthoImg2Global(unsigned int* G_red, unsigned int* G_green, unsigned int* G_blue, unsigned char* L_img, unsigned int* G_pointCounter,
							int* L_px1, int* L_px2, int* L_maxHeight, int* L_z,
							float *Wz, size_t stride_1ch, size_t stride_3ch, 
							const float camPosX, const float worldDimX, const float camPosY, const float worldDimY, const float GSD,
							const float minWorldPointX, const float minWorldPointY,
							cudaStream_t *stream)
{
	dim3 dimGrid(iDivUp(WIDTH, BLOCKSIZE_LocalWorld_x), iDivUp(HEIGHT, BLOCKSIZE_LocalWorld_y));
  	dim3 dimBlock(BLOCKSIZE_LocalWorld_x, BLOCKSIZE_LocalWorld_y);

  	float localWorldOriginX = (worldDimX * 0.5f) + camPosX;
	float localWorldOriginY = (worldDimY * 0.5f) + camPosY;
  	float offsetX = (localWorldOriginX - minWorldPointX) / GSD;
	float offsetY = (localWorldOriginY - minWorldPointY) / GSD;
	

	stride_1ch = stride_1ch >> 2;

  	G_red +=   int(round(offsetY) * stride_1ch + round(offsetX));
  	G_green += int(round(offsetY) * stride_1ch + round(offsetX));
  	G_blue +=  int(round(offsetY) * stride_1ch + round(offsetX));
  	G_pointCounter += int(round(offsetY) * stride_1ch + round(offsetX));

  	Wz += int(round(offsetY) * stride_1ch + round(offsetX));

  	Kernel_Projections<<<dimGrid, dimBlock, 0, (*stream)>>>(L_px1, L_px2, L_z, L_maxHeight, Wz, stride_1ch,
															camPosX, worldDimX, camPosY, worldDimY, GSD);

  	Kernel_CreateOrthoImg<<<dimGrid, dimBlock, 0, (*stream)>>>(	G_red, G_green, G_blue, L_img, G_pointCounter,
																L_px1, L_px2, L_maxHeight, L_z, GSD, stride_1ch, stride_3ch);
}