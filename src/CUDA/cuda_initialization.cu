#include "CUDA/cuda_wrapper.h"
#include "CUDA/cuda_defines.h"
#include <stdio.h>


inline int iDivUp(int a, int b){ return ((a % b) != 0) ? (a / b + 1) : (a / b); }

bool gpuAllocate2D(void** D_array, int width, int height, int bytes, size_t *stride)
{
	size_t widthB = width * bytes;

	cudaError_t cudaStatus;


	cudaStatus = cudaMallocPitch((void **) D_array, stride, widthB, height);
	if(cudaStatus != cudaSuccess)	{ printf("GPU allocation error!!\n"); return false; }

	return true;
}
void gpuDeallocate2D(void** D_array){   	cudaFree(*D_array);	  }
void gpuDeallocate(void** D_array){     	cudaFree(*D_array);	  }

void gpuPowerON(cudaStream_t *stream1){		cudaStreamCreate(stream1);		}
void gpuPowerOFF(cudaStream_t *stream1){	cudaStreamDestroy(*stream1);	}




__global__ void Kernel_gpuSetConstantUC(unsigned char *D_array, size_t stride, unsigned char constant, int width, int height)
{
	int tidx = blockIdx.x * blockDim.x + threadIdx.x;
	int tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ((tidx>=width) || (tidy>=height)) return;

	D_array[tidy*stride + tidx] = constant;
}
void gpuSetConstantUC(unsigned char *D_array, unsigned char constant, int width, int height, size_t stride, cudaStream_t *stream)
{
	dim3 dimGrid(iDivUp(width, BLOCKSIZE_GlobalWorld_x), iDivUp(height, BLOCKSIZE_GlobalWorld_y));
   	dim3 dimBlock(BLOCKSIZE_GlobalWorld_x, BLOCKSIZE_GlobalWorld_y);

	Kernel_gpuSetConstantUC<<<dimGrid, dimBlock, 0, (*stream)>>>(D_array, stride, constant, width, height);	
}

__global__ void Kernel_gpuSetConstantUI(unsigned int *D_array, size_t stride, unsigned int constant, int width, int height)
{
	int tidx = blockIdx.x * blockDim.x + threadIdx.x;
	int tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ((tidx>=width) || (tidy>=height)) return;

	D_array[tidy*stride + tidx] = constant;
}
void gpuSetConstantUI(unsigned int *D_array, unsigned int constant, int width, int height, size_t stride, cudaStream_t *stream)
{
	dim3 dimGrid(iDivUp(width, BLOCKSIZE_GlobalWorld_x), iDivUp(height, BLOCKSIZE_GlobalWorld_y));
   	dim3 dimBlock(BLOCKSIZE_GlobalWorld_x, BLOCKSIZE_GlobalWorld_y);
   	
	Kernel_gpuSetConstantUI<<<dimGrid, dimBlock, 0, (*stream)>>>(D_array, (stride>>2), constant, width, height);	
}

__global__ void Kernel_gpuSetConstantFLOAT(float *D_array, size_t stride, float constant, int width, int height)
{
	int tidx = blockIdx.x * blockDim.x + threadIdx.x;
	int tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ((tidx>=width) || (tidy>=height)) return;

	D_array[tidy*stride + tidx] = constant;
}
void gpuSetConstantFLOAT(float *D_array, float constant, int width, int height, size_t stride, cudaStream_t *stream)
{
	dim3 dimGrid(iDivUp(width, BLOCKSIZE_GlobalWorld_x), iDivUp(height, BLOCKSIZE_GlobalWorld_y));
   	dim3 dimBlock(BLOCKSIZE_GlobalWorld_x, BLOCKSIZE_GlobalWorld_y);

	Kernel_gpuSetConstantFLOAT<<<dimGrid, dimBlock, 0, (*stream)>>>(D_array, (stride>>2), constant, width, height);	
}

__global__ void Kernel_gpuSetConstantSI(int *D_array, size_t stride, int constant, int width, int height)
{
	int tidx = blockIdx.x * blockDim.x + threadIdx.x;
	int tidy = blockIdx.y * blockDim.y + threadIdx.y;

	if ((tidx>=width) || (tidy>=height)) return;

	D_array[tidy*stride + tidx] = constant;
}
void gpuSetConstantSI(int *D_array, int constant, int width, int height, size_t stride, cudaStream_t *stream)
{
	dim3 dimGrid(iDivUp(width, BLOCKSIZE_GlobalWorld_x), iDivUp(height, BLOCKSIZE_GlobalWorld_y));
   	dim3 dimBlock(BLOCKSIZE_GlobalWorld_x, BLOCKSIZE_GlobalWorld_y);
 
	Kernel_gpuSetConstantSI<<<dimGrid, dimBlock, 0, (*stream)>>>(D_array, (stride>>2), constant, width, height);	
}