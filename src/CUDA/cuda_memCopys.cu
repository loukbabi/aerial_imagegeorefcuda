#include "CUDA/cuda_wrapper.h"
#include <stdio.h>

bool gpuTOhost2D(void *host_array, const void* device_array, int width, int height, int bytes, size_t stride, size_t host_stride)
{
	cudaError_t cudaStatus;
	cudaStatus = cudaMemcpy2D(host_array, host_stride*bytes, device_array, stride, width*bytes, height, cudaMemcpyDeviceToHost);
	if(cudaStatus != cudaSuccess)	{ printf("Device to host error: %s!\n", cudaGetErrorString(cudaStatus)); return false; }

	return true;
}

bool hostTOgpu2D(const void *host_array, void* device_array, int width, int height, int bytes, size_t stride, size_t host_stride)
{
	cudaError_t cudaStatus;
	cudaStatus = cudaMemcpy2D(device_array, stride, host_array, host_stride*bytes, width*bytes, height, cudaMemcpyHostToDevice);
	if(cudaStatus != cudaSuccess)	{ printf("Host to device error: %s!\n", cudaGetErrorString(cudaStatus)); return false; }

	return true;	
}