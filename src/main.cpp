#include <iostream>
#include <stdio.h>
#include <Eigen/Dense>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

#include "CUDA/cuda_wrapper.h"
#include "executionLog.h"
#include "tictoc.h"
#include "CUDA/structures/cudaStructure.h"
#include "structures/snapshotStructure.h"
#include "structures/cameraStructure.h"
#include "structures/globalWorldStructure.h"
#include "structures/localWorldStructure.h"

#define NUM_OF_TESTS 1

int main(int argc, char const *argv[])
{
	cudaStream_t stream1;
	gpuPowerON(&stream1);

	int numOfInputImgs = readNumOfImgs();
	if(numOfInputImgs<=0) return -1;
	int *inputImageNumbers = new int[numOfInputImgs];
	readExecutionLog(inputImageNumbers);

	//Input Images
	Snapshot *inputSnapshotStream = new Snapshot[numOfInputImgs];
	//Read input Images
	for(int i = 0; i < numOfInputImgs; i++)
		if( !inputSnapshotStream[i].readSnapshot(INPUT_IMAGE_FOLDER, inputImageNumbers[i]) )   return -1;

	//Global World
	globalWorld _GlobalWorld;
	//Initialize Global World Container 
	if(!(_GlobalWorld.globalWorldInitialization(inputSnapshotStream, numOfInputImgs, &stream1)) ) return -1;//Check result after changing inversion

	//Local World
	localWorld _localWorld;
	//Initialize Local World Container 
	if(!_localWorld.allocate(inputSnapshotStream[0]))   return -1;

	//Start Georeference timer
	std::cout<<"-------Georeference--------"<<std::endl;
	tic();

	for(int testCount = 0; testCount < NUM_OF_TESTS; testCount++ )
	{
		//Loop through every input image
		for(int numOfimg = 0; numOfimg < numOfInputImgs; numOfimg++)
		{
			//Calculate field of view and focal length
			//FocaLength is calculated using the CELL_SIZE (can be any value) and the dimensions of the image. This will be not used once focal length become an input variable.
			float tanFOVdiv2 = tan( DEG2RAD(inputSnapshotStream[numOfimg].inputMetadata.FOV_angle) / 2 );
			float focaLength = (CELL_SIZE * (inputSnapshotStream[numOfimg].inputImg.rows / 2) ) / tanFOVdiv2;

			//Set basic camera values for current snapshot (6DoF and camera intrinsics)
			_localWorld._camera.setCameraValues(inputSnapshotStream[numOfimg], focaLength, CELL_SIZE_INV, CELL_SIZE_INV);
			_localWorld.L_maxHeight.setConstant( (int) -100, &stream1);

			_localWorld.sendProjMat2GPU();
			if(!inputSnapshotStream[numOfimg].sendImgTOgpu()) return -1;

			_localWorld.AddOrthoImg2Global(_GlobalWorld, inputSnapshotStream[numOfimg], &stream1);

		}
	}

	//End Georeference timer
	toc(numOfInputImgs*NUM_OF_TESTS);
	std::cout<<"---------------------------"<<std::endl;

	if(!_GlobalWorld.GetImgFromGPU(&stream1)) return -1;


	char fullPath[128];
	sprintf(fullPath, "%s/globalOrthoImg%d.%s", OUTPUT_FOLDER, numOfInputImgs, IMG_FORMAT);
	printf("Writing Global image: %s\n", fullPath);
	imwrite(fullPath, _GlobalWorld.globalOrthoImage);

	//Store point cloud
	#if STORE_GLOBAL_POINT_CLOUD
		_GlobalWorld.storePointCloud();
	#endif



	//Free memory
	for(int i = 0; i < numOfInputImgs; i++)
	{
		inputSnapshotStream[i].releaseSnapshot();
	}
	_GlobalWorld.deallocateGlobalOutput();
	_localWorld.deallocate();

	// delete[] _localWorld;
	delete[] inputSnapshotStream;
	delete[] inputImageNumbers;
	gpuPowerOFF(&stream1);

	return 0;
}
