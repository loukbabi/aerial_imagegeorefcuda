#include <Eigen/Dense>

#include "CUDA/structures/cudaStructure.h"
#include "structures/snapshotStructure.h"
#include "structures/cameraStructure.h"

#include "defines.h"


void camera::setCameraValues(Snapshot &_snapshot, float flocaLength, float kWidth, float kHeight)
{
	cameraValues.head(3) = (_snapshot.inputMetadata.UAVpossition);
	cameraValues.segment(3,3) = (_snapshot.inputMetadata.UAVorientation);
	cameraValues(6) = (flocaLength);
	cameraValues(7) = (_snapshot.inputImg.cols + 1) / 2;
	cameraValues(8) = (_snapshot.inputImg.rows + 1) / 2;
	cameraValues(9) = (kWidth);
	cameraValues(10) = (kHeight);

	projectionMatrixC2W_flatWorld();
	projectionMatrixW2C();
}

void camera::projectionMatrixC2W_flatWorld()
{
	Eigen::Matrix3f Rx, Ry, Rz, F, K, Rtemp;
	Eigen::Vector3f T;

	Rx << cos( DEG2RAD(cameraValues(5)) ), 	-sin( DEG2RAD(cameraValues(5)) ), 	0,
	      sin( DEG2RAD(cameraValues(5)) ), 	cos( DEG2RAD(cameraValues(5))),   	0,
	      0,						0,											1;
	Ry << cos( DEG2RAD(cameraValues(4)) ),	0,									-sin( DEG2RAD(cameraValues(4)) ),
		  0,								1,									0,
		  sin( DEG2RAD(cameraValues(4)) ),  0,									cos( DEG2RAD(cameraValues(4)) );
	Rz << 1,								0,									0,
		  0,								cos( DEG2RAD(cameraValues(3)) ),	-sin( DEG2RAD(cameraValues(3)) ),
		  0,								sin( DEG2RAD(cameraValues(3)) ),	cos( DEG2RAD(cameraValues(3)) );

	T << 0, 0, -cameraValues(2);
	Rtemp = Rz * Ry * Rx;
	T = Rtemp * T;
	Rtemp.col(2) = T;

	F << cameraValues(6), 	0, 			 		0,
		 0,					cameraValues(6), 	0,
		 0,					0,					-1;
	K << cameraValues(9),	0,					cameraValues(7),
		 0,					cameraValues(10),	cameraValues(8),
		 0,					0,					1;

	C2fW = (K * F * Rtemp).inverse();

	//R = R.inverse().eval();
}

void camera::projectionMatrixW2C()
{
	Eigen::Matrix3f Rx, Ry, Rz, F, K;
	Eigen::Vector3f T;

	Rx << cos( DEG2RAD(cameraValues(5)) ), 	-sin( DEG2RAD(cameraValues(5)) ), 	0,
			      sin( DEG2RAD(cameraValues(5)) ), 	cos( DEG2RAD(cameraValues(5))),   	0,
			      0,						0,											1;
	Ry << cos( DEG2RAD(cameraValues(4)) ),	0,									-sin( DEG2RAD(cameraValues(4)) ),
		  0,								1,									0,
		  sin( DEG2RAD(cameraValues(4)) ),  0,									cos( DEG2RAD(cameraValues(4)) );
	Rz << 1,								0,									0,
		  0,								cos( DEG2RAD(cameraValues(3)) ),	-sin( DEG2RAD(cameraValues(3)) ),
		  0,								sin( DEG2RAD(cameraValues(3)) ),	cos( DEG2RAD(cameraValues(3)) );

	T << 0, 0, -cameraValues(2);

	Eigen::Matrix<float, 3, 4> Rtemp;
	Rtemp.leftCols(3) = Rz * Ry * Rx;
	T = Rtemp.leftCols(3) * T;
	Rtemp.col(3) = T;

	F << cameraValues(6), 	0, 			 		0,
		 0,					cameraValues(6), 	0,
		 0,					0,					-1;
	K << cameraValues(9),	0,					cameraValues(7),
		 0,					cameraValues(10),	cameraValues(8),
		 0,					0,					1;

	W2C = K * F * Rtemp;
}

