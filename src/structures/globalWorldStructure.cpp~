#include <Eigen/Dense>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

#include "CUDA/cuda_wrapper.h"

#include "CUDA/structures/cudaStructure.h"
#include "structures/snapshotStructure.h"
#include "structures/cameraStructure.h"
#include "structures/globalWorldStructure.h"
#include "defines.h"

#include "readHeightMap.h"
#include "testers/write2txt.hpp"


elevationMap::elevationMap()
{
	int smallTileSizeX, smallTileSizeY;
	readTXT_size2(REAL_WORLD_HEIGHTMAP_PATH, &smallTileSizeX, &smallTileSizeY);

	//creating an offset at all four directions in order to avoid falling out of map
	

	#if INTERP_METHOD == 2
		tileSizeX = smallTileSizeX;
		tileSizeY = smallTileSizeY;

		worldDimX = smallTileSizeX * MAP_ACCURACY_X;
		worldDimY = smallTileSizeY * MAP_ACCURACY_Y;
		heights.resize(smallTileSizeY, smallTileSizeX);
		readTXT2_eigen(heights, REAL_WORLD_HEIGHTMAP_PATH);

		sendTOtextureMem(heights.data(), tileSizeX, tileSizeY);
	#else
		tileSizeX = MAP_EXTRA_OFFSET + smallTileSizeX + MAP_EXTRA_OFFSET;
		tileSizeY = MAP_EXTRA_OFFSET + smallTileSizeY + MAP_EXTRA_OFFSET;

//			heights.setZero(tileSizeY, tileSizeX);
		heights.setConstant(tileSizeY,tileSizeX,-1);
		Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> tempHeights;
		tempHeights.resize(smallTileSizeY, smallTileSizeX);
		readTXT2_eigen(tempHeights, REAL_WORLD_HEIGHTMAP_PATH);

		heights.block(MAP_EXTRA_OFFSET, MAP_EXTRA_OFFSET, smallTileSizeY, smallTileSizeX) = tempHeights;
	#endif

	halfTileSizeX = tileSizeX >> 1;
	halfTileSizeY = tileSizeY >> 1;
	accurasyX = MAP_ACCURACY_X;
	accurasyY = MAP_ACCURACY_Y;
}

bool elevationMap::sendTOtextureMem(float* H_data, int width, int height)
{
	return TextureBindingBilinearInterp(DT_heights, H_data, width, height, &DT_heights_stride);
}

bool globalWorld::allocateGlobalOutput(int width, int height, cudaStream_t *stream)
{
	globalOrthoImageValidValuesCount = new unsigned int[height*width];
	if(!D_globalOrthoImageValidValuesCount.allocate(height, width)) return false;
	D_globalOrthoImageValidValuesCount.setConstant( (unsigned int) 0, stream);

	z.resize(height, width);
	if(!D_z.allocate(height, width)) return false;
	D_z.setConstant( -1000.0f, stream);

	globalOrthoImage.create(height, width, CV_8UC3);
	if(!D_globalOrthoImage.allocate(height, width)) return false;

	D_globalOrthoImage.setConstantCannels( (unsigned int) 0, (unsigned int) 0, (unsigned int) 0, stream);

	globalValidOrthoValuesCount = 0;

	return true;
}
void globalWorld::deallocateGlobalOutput()
{
	delete[] globalOrthoImageValidValuesCount;
	globalOrthoImage.release();
	testHeights.heights.resize(0, 0);
	D_globalOrthoImageValidValuesCount.deallocate();
	D_globalOrthoImage.deallocate();
	D_z.deallocate();
}

bool globalWorld::GetImgFromGPU(cudaStream_t *stream)
{
	bool flag = true;
	D_globalOrthoImage.combineRGB(D_globalOrthoImageValidValuesCount.data(), stream);
	flag = flag && D_globalOrthoImage.getTOhost(globalOrthoImage.data, globalOrthoImage.step);
	flag = flag && D_z.getTOhost(z.data(), z.cols());
	flag = flag && D_globalOrthoImageValidValuesCount.getTOhost(globalOrthoImageValidValuesCount, z.cols());
	return flag;
}

bool globalWorld::storePointCloud()
{
	char fullPath[128];
	sprintf(fullPath, "%s/Global_pc.%s", OUTPUT_FOLDER, POINT_CLOUD_FORMAT);
	FILE * pFile;
	pFile = fopen (fullPath,"w");

	if(pFile)
	{
		unsigned int validCount = 0;

		float WorldOriginX = minWorldPointX;
		float WorldOriginY = maxWorldPointY;
		uchar *imgP = globalOrthoImage.data;
		int imgStep = globalOrthoImage.step;
		int height = globalOrthoImage.rows;
		int width = globalOrthoImage.cols;
		int red, green, blue;

		for(int i = 0; i < height; i++)
			for(int j = 0; j < width; j++)
				validCount += (globalOrthoImageValidValuesCount[(i * width + j)] != 0);

		fprintf(pFile, "COFF\n%d %d %d\n", validCount, 0, 0);

		for(int i = 0; i < height; i++)
			for(int j = 0; j < width; j++)
			{
				if( globalOrthoImageValidValuesCount[(i * width + j)] != 0 )
				{	
					blue  = int( imgP[ (i * width + j) * 3    ] );
					green = int( imgP[ (i * width + j) * 3 + 1] );
					red   = int( imgP[ (i * width + j) * 3 + 2] );

					fprintf(pFile, "%f %f %f %03d %03d %03d %03d\n",
							WorldOriginX + j*GSD, WorldOriginY - i*GSD,
							z(i,j), red, green, blue, 255);

				}
			}
	}else{
		printf("Could not create %s\n", fullPath);
	}

	fclose (pFile);
}

//Calculates the size of the global world (wont be needed in the actual implementation)
bool globalWorld::globalWorldInitialization(Snapshot* inputSnapshotStream, int inputImgNum, cudaStream_t *stream)
{
	float tempMinWorldPointX, tempMinWorldPointY, tempMaxWorldPointX, tempMaxWorldPointY;
	Eigen::Matrix3f C2W;
	Eigen::Vector3f pixel, TempWorldPoint;
	camera _camera;

	//For the first image
	float tanFOVdiv2 = tan( DEG2RAD(inputSnapshotStream[0].inputMetadata.FOV_angle) / 2 );

	float ySceneDistance = ( tanFOVdiv2 * inputSnapshotStream[0].inputMetadata.UAVpossition(2) ) * 2;
	maxGSD = minGSD = ySceneDistance/ inputSnapshotStream[0].inputImg.rows;
	float focaLength = (CELL_SIZE * (inputSnapshotStream[0].inputImg.rows / 2) ) / tanFOVdiv2;


	_camera.setCameraValues(inputSnapshotStream[0], focaLength, CELL_SIZE_INV, CELL_SIZE_INV );
	C2W = _camera.C2fW;

	//find which corners give the min and max world point (different for x and y)
	pixel.setOnes();// up-Left

	TempWorldPoint = C2W * pixel;
	TempWorldPoint /= TempWorldPoint(2);
	TempWorldPoint(0) = testHeights.halfTileSizeX * testHeights.accurasyX + TempWorldPoint(0) + inputSnapshotStream[0].inputMetadata.UAVpossition[0];
	TempWorldPoint(1) = testHeights.halfTileSizeY * testHeights.accurasyY + TempWorldPoint(1) + inputSnapshotStream[0].inputMetadata.UAVpossition[1];

	tempMinWorldPointX = tempMaxWorldPointX = TempWorldPoint(0);
	tempMinWorldPointY = tempMaxWorldPointY = TempWorldPoint(1);

	pixel << inputSnapshotStream[0].inputImg.cols, 1, 1;// up-Right
	TempWorldPoint = C2W * pixel;
	TempWorldPoint /= TempWorldPoint(2);
	TempWorldPoint(0) = testHeights.halfTileSizeX * testHeights.accurasyX + TempWorldPoint(0) + inputSnapshotStream[0].inputMetadata.UAVpossition[0];
	TempWorldPoint(1) = testHeights.halfTileSizeY * testHeights.accurasyY + TempWorldPoint(1) + inputSnapshotStream[0].inputMetadata.UAVpossition[1];

	if(TempWorldPoint(0)>tempMaxWorldPointX)
		tempMaxWorldPointX = TempWorldPoint(0);
	else if(TempWorldPoint(0)<tempMinWorldPointX)
		tempMinWorldPointX = TempWorldPoint(0);
	if(TempWorldPoint(1)>tempMaxWorldPointY)
		tempMaxWorldPointY = TempWorldPoint(1);
	else if(TempWorldPoint(1)<tempMinWorldPointY)
		tempMinWorldPointY = TempWorldPoint(1);

	pixel << 1, inputSnapshotStream[0].inputImg.rows, 1;// down-Left
	TempWorldPoint = C2W * pixel;
	TempWorldPoint /= TempWorldPoint(2);
	TempWorldPoint(0) = testHeights.halfTileSizeX * testHeights.accurasyX + TempWorldPoint(0) + inputSnapshotStream[0].inputMetadata.UAVpossition[0];
	TempWorldPoint(1) = testHeights.halfTileSizeY * testHeights.accurasyY + TempWorldPoint(1) + inputSnapshotStream[0].inputMetadata.UAVpossition[1];

	if(TempWorldPoint(0)>tempMaxWorldPointX)
		tempMaxWorldPointX = TempWorldPoint(0);
	else if(TempWorldPoint(0)<tempMinWorldPointX)
		tempMinWorldPointX = TempWorldPoint(0);
	if(TempWorldPoint(1)>tempMaxWorldPointY)
		tempMaxWorldPointY = TempWorldPoint(1);
	else if(TempWorldPoint(1)<tempMinWorldPointY)
		tempMinWorldPointY = TempWorldPoint(1);


	pixel << inputSnapshotStream[0].inputImg.cols, inputSnapshotStream[0].inputImg.rows, 1;// down-Right
	TempWorldPoint = C2W * pixel;
	TempWorldPoint /= TempWorldPoint(2);
	TempWorldPoint(0) = testHeights.halfTileSizeX * testHeights.accurasyX + TempWorldPoint(0) + inputSnapshotStream[0].inputMetadata.UAVpossition[0];
	TempWorldPoint(1) = testHeights.halfTileSizeY * testHeights.accurasyY + TempWorldPoint(1) + inputSnapshotStream[0].inputMetadata.UAVpossition[1];

	if(TempWorldPoint(0)>tempMaxWorldPointX)
		tempMaxWorldPointX = TempWorldPoint(0);
	else if(TempWorldPoint(0)<tempMinWorldPointX)
		tempMinWorldPointX = TempWorldPoint(0);
	if(TempWorldPoint(1)>tempMaxWorldPointY)
		tempMaxWorldPointY = TempWorldPoint(1);
	else if(TempWorldPoint(1)<tempMinWorldPointY)
		tempMinWorldPointY = TempWorldPoint(1);

	//For the rest of the images do exactly the same to calculate the global max and min world point (different for x and y)
	float tempGSD;
	for(int numOfImg = 1; numOfImg < inputImgNum; numOfImg++)
	{
		tanFOVdiv2 = tan( DEG2RAD(inputSnapshotStream[numOfImg].inputMetadata.FOV_angle) / 2 );
		ySceneDistance = ( tanFOVdiv2 * inputSnapshotStream[numOfImg].inputMetadata.UAVpossition(2) ) * 2;
		tempGSD = ySceneDistance / inputSnapshotStream[numOfImg].inputImg.rows;
		if(tempGSD>maxGSD)
			maxGSD = tempGSD;
		else if(tempGSD<minGSD)
			minGSD = tempGSD;

		focaLength = (CELL_SIZE * (inputSnapshotStream[numOfImg].inputImg.rows / 2) ) / tanFOVdiv2;

		_camera.setCameraValues(inputSnapshotStream[numOfImg], focaLength, CELL_SIZE_INV, CELL_SIZE_INV);
		C2W = _camera.C2fW;

		pixel.setOnes();// up-Left

		TempWorldPoint = C2W * pixel;
		TempWorldPoint /= TempWorldPoint(2);
		TempWorldPoint(0) = testHeights.halfTileSizeX * testHeights.accurasyX + TempWorldPoint(0) + inputSnapshotStream[numOfImg].inputMetadata.UAVpossition[0];
		TempWorldPoint(1) = testHeights.halfTileSizeY * testHeights.accurasyY + TempWorldPoint(1) + inputSnapshotStream[numOfImg].inputMetadata.UAVpossition[1];

		if(TempWorldPoint(0)>tempMaxWorldPointX)
			tempMaxWorldPointX = TempWorldPoint(0);
		else if(TempWorldPoint(0)<tempMinWorldPointX)
			tempMinWorldPointX = TempWorldPoint(0);
		if(TempWorldPoint(1)>tempMaxWorldPointY)
			tempMaxWorldPointY = TempWorldPoint(1);
		else if(TempWorldPoint(1)<tempMinWorldPointY)
			tempMinWorldPointY = TempWorldPoint(1);

		pixel << inputSnapshotStream[numOfImg].inputImg.cols, 1, 1;// up-Right
		TempWorldPoint = C2W * pixel;
		TempWorldPoint /= TempWorldPoint(2);
		TempWorldPoint(0) = testHeights.halfTileSizeX * testHeights.accurasyX + TempWorldPoint(0) + inputSnapshotStream[numOfImg].inputMetadata.UAVpossition[0];
		TempWorldPoint(1) = testHeights.halfTileSizeY * testHeights.accurasyY + TempWorldPoint(1) + inputSnapshotStream[numOfImg].inputMetadata.UAVpossition[1];

		if(TempWorldPoint(0)>tempMaxWorldPointX)
			tempMaxWorldPointX = TempWorldPoint(0);
		else if(TempWorldPoint(0)<tempMinWorldPointX)
			tempMinWorldPointX = TempWorldPoint(0);
		if(TempWorldPoint(1)>tempMaxWorldPointY)
			tempMaxWorldPointY = TempWorldPoint(1);
		else if(TempWorldPoint(1)<tempMinWorldPointY)
			tempMinWorldPointY = TempWorldPoint(1);

		pixel << 1, inputSnapshotStream[numOfImg].inputImg.rows, 1;// down-Left
		TempWorldPoint = C2W * pixel;
		TempWorldPoint /= TempWorldPoint(2);
		TempWorldPoint(0) = testHeights.halfTileSizeX * testHeights.accurasyX + TempWorldPoint(0) + inputSnapshotStream[numOfImg].inputMetadata.UAVpossition[0];
		TempWorldPoint(1) = testHeights.halfTileSizeY * testHeights.accurasyY + TempWorldPoint(1) + inputSnapshotStream[numOfImg].inputMetadata.UAVpossition[1];

		if(TempWorldPoint(0)>tempMaxWorldPointX)
			tempMaxWorldPointX = TempWorldPoint(0);
		else if(TempWorldPoint(0)<tempMinWorldPointX)
			tempMinWorldPointX = TempWorldPoint(0);
		if(TempWorldPoint(1)>tempMaxWorldPointY)
			tempMaxWorldPointY = TempWorldPoint(1);
		else if(TempWorldPoint(1)<tempMinWorldPointY)
			tempMinWorldPointY = TempWorldPoint(1);

		pixel << inputSnapshotStream[numOfImg].inputImg.cols, inputSnapshotStream[numOfImg].inputImg.rows, 1;// down-Right
		TempWorldPoint = C2W * pixel;
		TempWorldPoint /= TempWorldPoint(2);
		TempWorldPoint(0) = testHeights.halfTileSizeX * testHeights.accurasyX + TempWorldPoint(0) + inputSnapshotStream[numOfImg].inputMetadata.UAVpossition[0];
		TempWorldPoint(1) = testHeights.halfTileSizeY * testHeights.accurasyY + TempWorldPoint(1) + inputSnapshotStream[numOfImg].inputMetadata.UAVpossition[1];

		if(TempWorldPoint(0)>tempMaxWorldPointX)
			tempMaxWorldPointX = TempWorldPoint(0);
		else if(TempWorldPoint(0)<tempMinWorldPointX)
			tempMinWorldPointX = TempWorldPoint(0);
		if(TempWorldPoint(1)>tempMaxWorldPointY)
			tempMaxWorldPointY = TempWorldPoint(1);
		else if(TempWorldPoint(1)<tempMinWorldPointY)
			tempMinWorldPointY = TempWorldPoint(1);
	}

	//Find the maximum global world point
	float worldRangeX, worldRangeY;
	int globalImageRangeX, globalImageRangeY;
	#if PRINT_WITH_MAX_GSD
		#if USE_USERS_GSD
			maxGSD = USER_GSD;
		#endif
		GSD = maxGSD;
		worldRangeX = tempMaxWorldPointX - tempMinWorldPointX + maxGSD;
		worldRangeY = tempMaxWorldPointY - tempMinWorldPointY + maxGSD;
		globalImageRangeX = ceil(worldRangeX/maxGSD);
		globalImageRangeY = ceil(worldRangeY/maxGSD);
	#else
		#if USE_USERS_GSD
			minGSD = USER_GSD;
		#endif
		GSD = minGSD;
		worldRangeX = tempMaxWorldPointX - tempMinWorldPointX + minGSD;
		worldRangeY = tempMaxWorldPointY - tempMinWorldPointY + minGSD;
		globalImageRangeX = ceil(worldRangeX/minGSD);
		globalImageRangeY = ceil(worldRangeY/minGSD);
	#endif

	if(!allocateGlobalOutput(globalImageRangeX , globalImageRangeY, stream)) return false;
	minWorldPointX = tempMinWorldPointX;
	minWorldPointY = tempMinWorldPointY;
	maxWorldPointY = tempMaxWorldPointY;
	maxWorldPointX = tempMaxWorldPointX;

	return true;
}

void globalWorld::copyPanoToGlobalImg(cv::Mat &pano)
{
	cv::Size s = globalOrthoImage.size();
	cv::resize(pano, globalOrthoImage, s, 0, 0, cv::INTER_LINEAR);
}
