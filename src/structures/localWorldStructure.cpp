#include <Eigen/Dense>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

#include "CUDA/cuda_wrapper.h"

#include "CUDA/structures/cudaStructure.h"
#include "structures/snapshotStructure.h"
#include "structures/cameraStructure.h"
#include "structures/globalWorldStructure.h"
#include "interpolation.hpp"
#include "structures/localWorldStructure.h"
#include "testers/write2txt.hpp"

bool worldPoints::allocate(int sizeY, int sizeX)
{
	bool flag = true;
	x.resize(sizeY, sizeX);
	y.resize(sizeY, sizeX);
	z.resize(sizeY, sizeX);

	flag = flag && D_x.allocate(sizeY, sizeX);
	flag = flag && D_y.allocate(sizeY, sizeX);
	flag = flag && D_z.allocate(sizeY, sizeX);

	return flag;
}
void worldPoints::deallocate()
{
	x.resize(0, 0);
	y.resize(0, 0);
	z.resize(0, 0);

	D_x.deallocate();
	D_z.deallocate();
	D_y.deallocate();
}


bool pixelIDs::allocate(int sizeY, int sizeX)
{
	bool flag = true;
	x.resize(sizeY, sizeX);
	y.resize(sizeY, sizeX);

	flag = flag && D_x.allocate(sizeY, sizeX);
	flag = flag && D_y.allocate(sizeY, sizeX);

	return flag;
}
void pixelIDs::deallocate()
{
	x.resize(0, 0);
	y.resize(0, 0);

	D_x.deallocate();
	D_y.deallocate();
}

bool projectedImg::allocate(int sizeY, int sizeX)
{
	bool flag = true;
	#if USE_STITCHED_FOR_POINT_CLOUD == 0
		validOrthoValuesCount = sizeY * sizeX;
	#endif

	perspIds.allocate(sizeY, sizeX);
	validOrthoValues.resize(sizeY, sizeX);
	flag = flag && D_validOrthoValues.allocate(sizeY, sizeX);

	#if USE_HEIGHT_PRIORITY
		maxElevation.resize(sizeY, sizeX);
		flag = flag && D_maxElevation.allocate(sizeY, sizeX);
	#endif

	firstValidLeft = -1000000000;
	firstValidUp = -1000000000;
	firstValidRight = 1000000000;
	firstValidDown = 1000000000;

	return flag;
}
void projectedImg::deallocate()
{
	perspIds.deallocate();
	validOrthoValues.resize(0, 0);

	D_validOrthoValues.deallocate();

	#if USE_HEIGHT_PRIORITY
		maxElevation.resize(0, 0);
		D_maxElevation.deallocate();
	#endif
}


bool localWorld::allocate(Snapshot &_snapshot)
{
	bool flag = true;
	flag = flag && _worldPoints.allocate(_snapshot.inputImg.rows, _snapshot.inputImg.cols);
	flag = flag && pixelsLog.allocate(_snapshot.inputImg.rows, _snapshot.inputImg.cols);
	orthoImage.create(_snapshot.inputImg.rows, _snapshot.inputImg.cols, CV_8UC3);

	flag = flag && D_px1.allocate(_snapshot.inputImg.rows, _snapshot.inputImg.cols);
	flag = flag && D_px2.allocate(_snapshot.inputImg.rows, _snapshot.inputImg.cols);
	flag = flag && L_maxHeight.allocate(_snapshot.inputImg.rows, _snapshot.inputImg.cols);
	flag = flag && L_z.allocate(_snapshot.inputImg.rows, _snapshot.inputImg.cols);

	return flag;
}
void localWorld::deallocate()
{
	orthoImage.release();
	_worldPoints.deallocate();
	pixelsLog.deallocate();

	D_px1.deallocate();
	D_px2.deallocate();
	L_maxHeight.deallocate();
	L_z.deallocate();

}

void localWorld::sendProjMat2GPU()
{
	cuda_C2fW_cpy2Const(_camera.C2fW.data());
	cuda_W2C_cpy2Const(_camera.W2C.data());
}

void localWorld::cam2flatWorld_proj(cudaStream_t *stream)
{
	cuda_cam2flatWorld_proj(_worldPoints.D_x.data(), _worldPoints.D_y.data(), _worldPoints.D_x.stride(), stream);

}
void localWorld::AddOrthoImg2Global(globalWorld &_GlobalWorld, Snapshot &inSnapshot, cudaStream_t *stream)
{
	cuda_AddOrthoImg2Global(_GlobalWorld.D_globalOrthoImage.D_imgR.data(),_GlobalWorld.D_globalOrthoImage.D_imgG.data(), _GlobalWorld.D_globalOrthoImage.D_imgB.data(), inSnapshot.D_inputImg.data(), _GlobalWorld.D_globalOrthoImageValidValuesCount.data(),
							D_px1.data(), D_px2.data(), L_maxHeight.data(), L_z.data(),
							_GlobalWorld.D_z.data(), _GlobalWorld.D_globalOrthoImage.D_imgR.stride(), inSnapshot.D_inputImg.stride(),
							_camera.cameraValues(0), _GlobalWorld.testHeights.worldDimX, _camera.cameraValues(1), _GlobalWorld.testHeights.worldDimY, 
							_GlobalWorld.GSD, _GlobalWorld.minWorldPointX, _GlobalWorld.minWorldPointY,
							stream);
}

void localWorld::findFlatWorldArea(Snapshot &_snapshot)
{
	int height = _snapshot.inputImg.rows - 1, width = _snapshot.inputImg.cols - 1;

	float pt1, pt2, pt3;
	float a00, a01, a02,
		  a10, a11, a12,
		  a20, a21, a22;
	a00 = _camera.C2fW(0,0);	a01 = _camera.C2fW(0,1);	a02 = _camera.C2fW(0,2);
	a10 = _camera.C2fW(1,0);	a11 = _camera.C2fW(1,1);	a12 = _camera.C2fW(1,2);
	a20 = _camera.C2fW(2,0);	a21	= _camera.C2fW(2,1);	a22	= _camera.C2fW(2,2);

	pt3 = a22;
	pt1 = (a02) / pt3;
	pt2 = (a12) / pt3;
	float leftTopPointX = pt1, leftTopPointY = pt2;

	pt3 = a21 * height + a22;
	pt1 = (a01 * height + a02) / pt3;
	pt2 = (a11 * height + a12) / pt3;
	float leftBottomPointX = pt1, leftBottomPointY = pt2;

	pt3 = a20 * width + a22;
	pt1 = (a00 * width + a02) / pt3;
	pt2 = (a10 * width + a12) / pt3;
	float rightTopPointX = pt1, rightTopPointY = pt2;

	pt3 = a20 * width + a21 * height + a22;
	pt1 = (a00 * width + a01 * height + a02) / pt3;
	pt2 = (a10 * width + a11 * height + a12) / pt3;
	float rightBottomPointX = pt1, rightBottomPointY = pt2;

	_worldPoints.minX = std::min( leftTopPointX,
						std::min( leftBottomPointX,
						std::min( rightTopPointX, rightBottomPointX )));
	_worldPoints.maxX = std::max( leftTopPointX,
						std::max( leftBottomPointX,
						std::max( rightTopPointX, rightBottomPointX )));
	_worldPoints.minY = std::min( leftTopPointY,
						std::min( leftBottomPointY,
						std::min( rightTopPointY, rightBottomPointY )));
	_worldPoints.maxY = std::max( leftTopPointY,
						std::max( leftBottomPointY,
						std::max( rightTopPointY, rightBottomPointY )));
}

void localWorld::test_heights_interp(elevationMap &worldHeights, cudaStream_t *stream)
{
	Eigen::Array<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> temp_heights;
	temp_heights.resize(HEIGHT, WIDTH);
	cuda_array<float> D_InterpHeights;
	D_InterpHeights.allocate(HEIGHT, WIDTH);

	cuda_Heights_interp(D_InterpHeights.data(), _worldPoints.D_x.data(), _worldPoints.D_y.data(), 
						_camera.cameraValues(0), worldHeights.worldDimX,
						_camera.cameraValues(1), worldHeights.worldDimY,
						D_InterpHeights.stride(), stream);
	D_InterpHeights.getTOhost(temp_heights.data(), WIDTH);
}

bool localWorld::addHeights(elevationMap &worldHeights, cudaStream_t *stream)
{
	int blockWidth = round(_worldPoints.maxX - _worldPoints.minX)/worldHeights.accurasyX;
	int blockHeight = round(_worldPoints.maxY - _worldPoints.minY)/worldHeights.accurasyY;

	int startPointX = round(worldHeights.halfTileSizeX + ((_camera.cameraValues(0) + _worldPoints.minX)/worldHeights.accurasyX));
	int startPointY = round(worldHeights.halfTileSizeY + ((_camera.cameraValues(1) + _worldPoints.minY)/worldHeights.accurasyY));

	test_heights_interp(worldHeights, stream);

	return true;
}
