#include <Eigen/Dense>
#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>

#include "CUDA/cuda_wrapper.h"
#include "CUDA/structures/cudaStructure.h"
#include "structures/snapshotStructure.h"
#include "readFile.h"
#include "defines.h"


bool Snapshot::readImg(const char* inputPath, int imgNum)
{
	char fullPath[128];
	sprintf(fullPath, "%s/%08d.%s", inputPath, imgNum, IMG_FORMAT);
	inputImg = cv::imread(fullPath, CV_LOAD_IMAGE_COLOR);
	if(!inputImg.data)
	{
		std::cout <<  "Could not open or find image " << imgNum << " ." << std::endl ;
		return false;
	}
	return true;
}

bool Snapshot::readMetadata(const char* inputPath, int imgNum)
{
	char fullPath[128];
	sprintf(fullPath, "%s/%08d.%s", inputPath, imgNum, METADATA_FORMAT);
	return readTXT(fullPath, inputMetadata.UAVorientation, inputMetadata.UAVpossition, &inputMetadata.FOV_angle );
}

bool Snapshot::readSnapshot(const char* inputPath, int imgNum)
{
	bool flag;
	flag = readImg(inputPath, imgNum);
	flag = readMetadata(inputPath, imgNum) && flag;
	D_inputImg.allocate(inputImg.rows, inputImg.cols);
	return flag;
}

bool Snapshot::sendImgTOgpu()
{
	return D_inputImg.sendToGPU(inputImg.data, inputImg.step);
}

void Snapshot::releaseSnapshot()
{
	inputImg.release();
	D_inputImg.deallocate();
}

Snapshot::Snapshot() {}
Snapshot::~Snapshot() {}
